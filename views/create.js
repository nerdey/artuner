 $(document).ready(function() {

	var wrapper = $('<div/>').css({height:0,width:0,'overflow':'hidden'});
	var fileInput = $('#file-input').wrap(wrapper);

	fileInput.change(function(){
	    $this = $(this);
	    $('#filepath').text($this.val());
	})

	$('#filebutton').click(function(){
	    fileInput.click();

	}).show();
});

