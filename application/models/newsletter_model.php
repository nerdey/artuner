<?php
class Newsletter_model extends CI_Model {

	public function __construct()
	{
		$this->load->database();
	}

	/**
	* Get a newsletter
	*
	* @param int $id the id of the newsletter
	* @return array
	*
	*/
	public function get_newsletter($id = FALSE)
	{
		if ($id === FALSE)
		{
			$this->db->order_by('id', 'desc');
			$query = $this->db->get('newsletter');
			return $query->result_array();
		}

		$this->db->order_by('id', 'desc');
		$query = $this->db->get_where('newsletter', array('id' => $id));
		return $query->row_array();
	}

	/**
	* Set a newsletter: insert it new to db or if $id is given, edit an existing newsletter
	*
	* @param int $id the id of the newsletter
	* @return bool was db operation successful
	*
	*/
	public function set_newsletter( $id = FALSE )
	{
		$this->load->helper('url');
		$this->load->helper('date');
			
		$data = array(
			'title' => $this->input->post('title'),
			'message' => $this->input->post('message'),
			'date' => date("Y-m-d H:i:s", time())
		);
		
		if ( $id === FALSE ){
			return $this->db->insert('newsletter', $data);
		}
		else{
			$this->db->where('id', $id);
			return $this->db->update('newsletter', $data); 
		}
	}

	/**
	* Delete a newsletter and all data that is linked to that newsletter
	*
	* @param int $nl_id the id of the newsletter
	*
	*/
	public function delete_newsletter( $nl_id )
	{
		$this->delete_all_artworks($nl_id);
		$this->delete_all_insights($nl_id);
		$this->delete_all_current_curations($nl_id);
		$this->delete_all_last_curations($nl_id);

		$this->db->where('id', $nl_id);
		return $this->db->delete('newsletter');	
	}

	/**
	* Get a current curation
	*
	* @param int $nl_id the id of the newsletter containing the current curation
	* @return array
	*
	*/
	public function get_current_curations( $nl_id = FALSE, $cu_id = FALSE )
	{
		if ($nl_id === FALSE && $cu_id === FALSE)
		{
			$query = $this->db->get('current_curation');
			return $query->result_array();
		}
		else if ( $cu_id === FALSE ){
			$this->db->order_by('id', 'desc');
			$query = $this->db->get_where('current_curation', array('newsletter' => $nl_id), 1);
			return $query->result_array();
		}

		$query = $this->db->get_where('current_curation', array('id' => $cu_id), 1);
		return $query->result_array();
	}

	/**
	* Set a current curation: insert it new to db or if $id is given, edit an existing current curation
	*
	* @param int $nl_id the id of the newsletter
	* @param string $imagepath path to curator image
	* @param int $id the id of the current curation
	* @return array
	*
	*/
	public function set_current_curation( $nl_id, $imagepath, $id = FALSE )
	{	
		$data = array(
			'newsletter' => $nl_id,
			'link' =>  prep_url($this->input->post('link')), 
			'heading' => $this->input->post('heading'),
			'subhead' => $this->input->post('subhead'),
			'excerpt' => $this->input->post('excerpt'), 
			'curator_name' => $this->input->post('curator_name'), 
			'curator_link' => prep_url($this->input->post('curator_link'))
		);
		if($imagepath){
			$data['curator_image'] = $imagepath;
		}
		if ( $id === FALSE ){
			return $this->db->insert('current_curation', $data);
		}
		else{
			$this->db->where('id', $id);
			return $this->db->update('current_curation', $data); 
		}
	}

	/**
	* Delete a current curation
	*
	* @param int $id the id of the current curation
	* @return bool if deletion was successful
	*/
	public function delete_current_curation( $id )
	{
		$this->db->where('id', $id);
		return $this->db->delete('current_curation'); 
	}

	/**
	* Delete all current curations for given newsletter id
	*
	* @param int $nl_id the id of the newsletter
	* @return bool if deletion was successful
	*/
	public function delete_all_current_curations( $nl_id ){
		$this->db->where('newsletter', $nl_id);
		return $this->db->delete('current_curation'); 
	}

	/**
	* Get a last curation
	*
	* @param int $id the id of the newsletter containing the last curation
	* @return array
	*
	*/
	public function get_last_curations( $nl_id = FALSE, $cu_id = FALSE )
	{
		if ($nl_id === FALSE && $cu_id === FALSE)
		{
			$query = $this->db->get('last_curation');
			return $query->result_array();
		}
		else if ( $cu_id === FALSE ){
			$this->db->order_by('id', 'desc');
			$query = $this->db->get_where('last_curation', array('newsletter' => $nl_id), 1);
			return $query->result_array();
		}

		$query = $this->db->get_where('last_curation', array('id' => $cu_id), 1);
		return $query->result_array();

	}


	/**
	* Set a last curation: insert it new to db or if $id is given, edit an existing last curation
	*
	* @param int $nl_id the id of the newsletter
	* @param string $imagepath path to curator image
	* @param int $id the id of the last curation
	* @return array
	*
	*/
	public function set_last_curation( $nl_id, $imagepath, $id = FALSE )
	{	

		$data = array(
			'newsletter' => $nl_id,
			'link' => prep_url($this->input->post('link')), 
			'heading' => $this->input->post('heading'),
			'subhead' => $this->input->post('subhead'),
			'excerpt' => $this->input->post('excerpt'), 
			'curator_name' => $this->input->post('curator_name'), 
			'curator_link' => prep_url($this->input->post('curator_link'))
		);
		if($imagepath){
			$data['curator_image'] = $imagepath;
		}
		if ( $id === FALSE ){
			return $this->db->insert('last_curation', $data);
		}
		else{
			$this->db->where('id', $id);
			return $this->db->update('last_curation', $data); 
		}
	}

	/**
	* Delete a last curation
	*
	* @param int $id the id of the last curation
	* @return bool if deletion was successful
	*/
	public function delete_last_curation( $id )
	{
		$this->db->where('id', $id);
		return $this->db->delete('last_curation'); 
	}

	/**
	* Delete all last curations for given newsletter id
	*
	* @param int $nl_id the id of the newsletter
	* @return bool if deletion was successful
	*/
	public function delete_all_last_curations( $nl_id ){
		$this->db->where('newsletter', $nl_id);
		return $this->db->delete('last_curation'); 
	}

	/**
	* Get insights either for given newsletter ID or for ID of the Insight itself.
	*
	* @param int $nl_id the id if the newsletter containing the insights
	* @param int $in_id the id of the insight
	* @return array
	*
	*/
	public function get_insights($nl_id = FALSE, $in_id = FALSE)
	{
		if ($nl_id === FALSE && $in_id === FALSE)
		{
			$query = $this->db->get('insight');
			return $query->result_array();
		}
		else if ( $in_id === FALSE ){
			$this->db->order_by('id', 'asc');
			$query = $this->db->get_where('insight', array('newsletter' => $nl_id), 3);
			return $query->result_array();
		}
		$query = $this->db->get_where('insight', array('id' => $in_id), 1);
		return $query->result_array();

	}


	/**
	* Set an insight: insert it new to db or if $id is given, edit an existing insight
	*
	* @param int $nl_id the id of the newsletter
	* @param int $in_id the id of the insight
	* @return array
	*
	*/
	public function set_insight( $nl_id, $in_id = FALSE )
	{
		$data = array(
			'newsletter' => $nl_id,
			'category' => $this->input->post('category'), 
			'heading' => $this->input->post('heading'),
			'link' => prep_url($this->input->post('link')), 
			'excerpt' => $this->input->post('excerpt')
		);

		if ( $in_id === FALSE ){
			return $this->db->insert('insight', $data);
		}
		else{
			$this->db->where('id', $in_id);
			return $this->db->update('insight', $data); 
		}
	}

	/**
	* Delete an insight
	*
	* @param int $id the id of the insight
	* @return bool if deletion was successful
	*/
	public function delete_insight( $id )
	{
		$this->db->where('id', $id);
		return $this->db->delete('insight'); 
	}

	/**
	* Delete all insights for given newsletter id
	*
	* @param int $nl_id the id of the newsletter
	* @return bool if deletion was successful
	*/
	public function delete_all_insights( $nl_id ){
		$this->db->where('newsletter', $nl_id);
		return $this->db->delete('insight'); 
	}

	/**
	* Get artworks
	*
	* @param int $nl_id the id of the newsletter containing the artworks
	* @param int $aw_id the artwork's id
	* @return array
	*
	*/
	public function get_artworks( $nl_id = FALSE, $aw_id = FALSE )
	{
		if ( $nl_id === FALSE && $aw_id === FALSE )
		{
			$query = $this->db->get('artwork');
			return $query->result_array();
		}
		else if ( $aw_id === FALSE ){
			$this->db->order_by('id', 'asc');
			$query = $this->db->get_where('artwork', array('newsletter' => $nl_id), 6);
			return $query->result_array();
		}
		
		$query = $this->db->get_where('artwork', array('id' => $aw_id), 1);
		return $query->result_array();
	}


	/**
	* Set an artwork: insert it new to db or if $id is given, edit an existing artwork
	*
	* @param int $nl_id the id of the newsletter
	* @param string $imagepath path to image of artwork
 	* @param int $id the id of the artwork
	* @return array
	*
	*/
	public function set_artwork( $nl_id, $imagepath, $id = FALSE )
	{

		$data = array(
			'newsletter' => $nl_id,
			'title' => $this->input->post('title'), 
			'artistname' => $this->input->post('artistname'),
			'artistlink' => prep_url($this->input->post('artistlink')),
			'price' => $this->input->post('price'),
			'link' => prep_url($this->input->post('link'))
		);

		if($imagepath){
			$data['image'] = $imagepath;
		}

		if ( $id === FALSE ){
			return $this->db->insert('artwork', $data);
		}
		else{
			$this->db->where('id', $id);
			return $this->db->update('artwork', $data); 
		}	
	}


	/**
	* Delete an artwork
	*
	* @param int $id the id of the artwork
	* @return bool if deletion was successful
	*/
	public function delete_artwork( $id )
	{
		$this->db->where('id', $id);
		return $this->db->delete('artwork'); 
	}

	/**
	* Delete all artworks for given newsletter id
	*
	* @param int $nl_id the id of the newsletter
	* @return bool if deletion was successful
	*/
	public function delete_all_artworks( $nl_id ){
		$this->db->where('newsletter', $nl_id);
		return $this->db->delete('artwork'); 
	}
}

