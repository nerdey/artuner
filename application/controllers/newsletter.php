<?php
class Newsletter extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->model('newsletter_model');
		$this->load->helper('form');
		$this->load->library('form_validation');
		$this->load->helper('url');

		$config['upload_path'] = './uploads/';
		$config['allowed_types'] = 'gif|jpg|png';
		$config['max_size']	= '10000';
		$config['max_width']  = '10000';
		$config['max_height']  = '10000';

		$this->load->library('upload', $config);

		$data['url'] = base_url();
	}

	public function index()
	{		
		$data['url'] = base_url();
		$data['newsletters'] = $this->newsletter_model->get_newsletter();
		$data['title'] = 'All Newsletters';
		$this->load->view('templates/header', $data);
		$this->load->view('nl_builder/newsletters', $data);
		$this->load->view('templates/footer');

	}

	/**
	*  View a Newsletter
	*	
	* @param it $nl_id the Newsletter's id
	*/
	public function view($nl_id)
	{
		$data['url'] = base_url();
		$data['newsletter_item'] = $this->newsletter_model->get_newsletter($nl_id);
		$data['title'] = $data['newsletter_item']['title'];
		$data['date'] = date("d.m.Y", strtotime($data['newsletter_item']['date']));
		$data['id'] = $nl_id;
		$data['current_curations'] = $this->newsletter_model->get_current_curations($nl_id);
		$data['last_curations'] = $this->newsletter_model->get_last_curations($nl_id);
		$data['artworks'] = $this->newsletter_model->get_artworks($nl_id);
		$data['insights'] = $this->newsletter_model->get_insights($nl_id);
		$data['url'] = base_url();
		$data['is_complete'] = $this->is_complete($nl_id);

		if (empty($data['newsletter_item']))
		{
			show_404();
		}

		$this->load->view('templates/header_overview', $data);
		$this->load->view('nl_builder/overview', $data);
		$this->load->view('templates/footer');

	}

	/**
	* Create a Newsletter
	*
	* 
	*/
	public function create()
	{
		$data['title'] = 'Newsletters';
		$data['newsletters'] = $this->newsletter_model->get_newsletter();
		$data['url'] = base_url();
		
		$this->form_validation->set_rules('title', 'Title', 'required');
		$this->form_validation->set_rules('message', 'Welcome Message', 'required');
		
		if ($this->form_validation->run() === FALSE)
		{
			$this->load->view('templates/header', $data);	
			$this->load->view('nl_builder/newsletters', $data);
			$this->load->view('templates/footer');
			
		}
		else
		{
			$this->newsletter_model->set_newsletter();
			$data['newsletters'] = $this->newsletter_model->get_newsletter();
			$this->load->view('templates/header', $data);	
			$this->load->view('nl_builder/newsletters', $data);
			$this->load->view('templates/footer');
		}
	}

	/**
	* Edit Newsletter
	*
	* @param int $nl_id the id of the Newsletter
	*/
	public function edit_newsletter($nl_id){
		$data['subtitle'] = 'Edit Newsletter';
		$data['nl_id'] = $nl_id;
		$data['newsletter_item'] = $this->newsletter_model->get_newsletter($nl_id);
		$data['date'] = date("d.m.Y", strtotime($data['newsletter_item']['date']));
		$data['title'] = $data['newsletter_item']['title'];
		$data['url'] = base_url();
	
		$this->form_validation->set_rules('title', 'Title', 'required');
		$this->form_validation->set_rules('message', 'Welcome Message', 'required');

		if ($this->form_validation->run() === FALSE)
		{
			$this->load->view('templates/header', $data);	
			$this->load->view('nl_builder/edit_newsletter', $data);
			$this->load->view('templates/footer');
		}
		else
		{
			$this->newsletter_model->set_newsletter($nl_id);
			$data['newsletter_item'] = $this->newsletter_model->get_newsletter($nl_id);
			$this->load->view('templates/header', $data);	
			$this->load->view('nl_builder/edit_newsletter_success', $data);
			$this->load->view('templates/footer');
		}
	}

	/**
	* Delete a Newsletter
	*
	* @param int $nl_id the id of the newsletter
	*/
	public function delete_newsletter($nl_id){

		$data['subtitle'] = 'Edit Newsletter';
		$data['nl_id'] = $nl_id;
		$data['newsletter_item'] = $this->newsletter_model->get_newsletter($nl_id);
		$data['date'] = date("d.m.Y", strtotime($data['newsletter_item']['date']));
		$data['title'] = $data['newsletter_item']['title'];
		$data['url'] = base_url();

		if($this->newsletter_model->delete_newsletter($nl_id)){
			$this->load->view('templates/header', $data);	
			$this->load->view('nl_builder/delete_nl_success', $data);
			$this->load->view('templates/footer');
		}
		else{
			$this->load->view('templates/header', $data);	
			$this->load->view('nl_builder/edit_newsletter', $data);
			$this->load->view('templates/footer');
		}
	}


	/**
	* Create a current_curation
	*
	* @param int $nl_id the id of the Newsletter linked to the current curation
	*/
	public function create_current_curation($nl_id){
		$data['subtitle'] = 'Create Current Curation';
		$data['nl_id'] = $nl_id;
		$data['newsletter_item'] = $this->newsletter_model->get_newsletter($nl_id);
		$data['date'] = date("d.m.Y", strtotime($data['newsletter_item']['date']));
		$data['title'] = $data['newsletter_item']['title'];
		$data['url'] = base_url();

		$this->form_validation->set_rules('heading', 'Heading', 'required');
		$this->form_validation->set_rules('link', 'Link to Curation', 'required');
		$this->form_validation->set_rules('subhead', 'Subhead', 'required');
		$this->form_validation->set_rules('excerpt', 'Excerpt', 'required');
		$this->form_validation->set_rules('curator_name', 'Curator Name', 'required');
		$this->form_validation->set_rules('curator_link', 'Link to Curator profile', 'required');

		if ($this->form_validation->run() === FALSE ||  ! $this->upload->do_upload())
		{
			$error = $this->upload->display_errors();
			$this->load->view('templates/header', $data);	
			$this->load->view('nl_builder/create_current_curation', $data);
			$this->load->view('templates/footer');
			
		}
		else
		{
			$image_array = array('upload_data' => $this->upload->data());
			$imagepath = $data['url']."../uploads/".$image_array['upload_data']['file_name'];
			$this->resizeImage($image_array['upload_data']['full_path'], '70');
			$this->newsletter_model->set_current_curation($nl_id, $imagepath);
			$cu = $this->newsletter_model->get_current_curations($nl_id, null);
			$data['curation'] = $cu[0];
			$this->load->view('templates/header', $data);	
			$this->load->view('nl_builder/create_current_curation_success', $data);
			$this->load->view('templates/footer');
		}

	}

	/**
	* Edit current curation
	*
	* @param int $cu_id the id of the current curation
	*/
	public function edit_current_curation($cu_id){
		$data['subtitle'] = 'Edit Current Curation';
		$cu = $this->newsletter_model->get_current_curations(null, $cu_id);
		$data['curation'] = $cu[0];
		$nl_id = $data['curation']['newsletter'];
		$data['id'] = $cu_id;
		$data['nl_id'] = $nl_id;
		$data['newsletter_item'] = $this->newsletter_model->get_newsletter($nl_id);
		$data['date'] = date("d.m.Y", strtotime($data['newsletter_item']['date']));
		$data['title'] = $data['newsletter_item']['title'];
		$data['url'] = base_url();
	
		$this->form_validation->set_rules('heading', 'Heading', 'required');
		$this->form_validation->set_rules('link', 'Link to Curation', 'required');
		$this->form_validation->set_rules('subhead', 'Subhead', 'required');
		$this->form_validation->set_rules('excerpt', 'Excerpt', 'required');
		$this->form_validation->set_rules('curator_name', 'Curator Name', 'required');
		$this->form_validation->set_rules('curator_link', 'Link to Curator profile', 'required');

		if ($this->form_validation->run() === FALSE &&  ! $this->upload->do_upload())
		{
			$data['error'] = array('error' => $this->upload->display_errors());
			$this->load->view('templates/header', $data);	
			$this->load->view('nl_builder/edit_current_curation', $data);
			$this->load->view('templates/footer');
		}
		else if(!$this->upload->do_upload()){
			$this->newsletter_model->set_current_curation($nl_id, null, $cu_id);
			$cu = $this->newsletter_model->get_current_curations(null, $cu_id);
			$data['curation'] = $cu[0];
			$this->load->view('templates/header', $data);	
			$this->load->view('nl_builder/create_current_curation_success', $data);
			$this->load->view('templates/footer');
		}
		else
		{
			$image_array = array('upload_data' => $this->upload->data());
			$imagepath = $data['url']."../uploads/".$image_array['upload_data']['file_name'];
			$this->resizeImage($image_array['upload_data']['full_path'], '70');
			$this->newsletter_model->set_current_curation($nl_id, $imagepath, $cu_id);
			$cu = $this->newsletter_model->get_current_curations(null, $cu_id);
			$data['curation'] = $cu[0];
			$this->load->view('templates/header', $data);	
			$this->load->view('nl_builder/create_current_curation_success', $data);
			$this->load->view('templates/footer');
		}
	}

	/**
	* Delete a current Curation
	*
	* @param int $cu_id the id of the current curation
	*/
	public function delete_current_curation($cu_id){

		$data['subtitle'] = 'Edit Insight';
		$data['curation'] = $this->newsletter_model->get_current_curations(null, $cu_id);
		$nl_id = $data['curation'][0]['newsletter'];
		$data['nl_id'] = $nl_id;
		$data['newsletter_item'] = $this->newsletter_model->get_newsletter($nl_id);
		$data['title'] = $data['newsletter_item']['title'];
		$data['date'] = date("d.m.Y", strtotime($data['newsletter_item']['date']));
		$data['url'] = base_url();

		if($this->newsletter_model->delete_current_curation($cu_id)){
			$this->load->view('templates/header', $data);	
			$this->load->view('nl_builder/delete_success', $data);
			$this->load->view('templates/footer');
		}
		else{
			$this->load->view('templates/header', $data);	
			$this->load->view('nl_builder/edit_current_curation', $data);
			$this->load->view('templates/footer');
		}
	}

	/**
	* Create a last curation
	*
	* @param int $nl_id the id of the Newsletter linked to the last curation
	*/
	public function create_last_curation($nl_id){
		$data['subtitle'] = 'Create Last Curation';
		$data['nl_id'] = $nl_id;
		$data['newsletter_item'] = $this->newsletter_model->get_newsletter($nl_id);
		$data['title'] = $data['newsletter_item']['title'];
		$data['date'] = date("d.m.Y", strtotime($data['newsletter_item']['date']));
		$data['url'] = base_url();

		$this->form_validation->set_rules('heading', 'Heading', 'required');
		$this->form_validation->set_rules('link', 'Link to Curation', 'required');
		$this->form_validation->set_rules('subhead', 'Subhead', 'required');
		$this->form_validation->set_rules('excerpt', 'Excerpt', 'required');
		$this->form_validation->set_rules('curator_name', 'Curator Name', 'required');
		$this->form_validation->set_rules('curator_link', 'Link to Curator profile', 'required');

		if ($this->form_validation->run() === FALSE ||  ! $this->upload->do_upload())
		{
			$this->load->view('templates/header', $data);	
			$this->load->view('nl_builder/create_last_curation', $data);
			$this->load->view('templates/footer');
			
		}
		else
		{
			$image_array = array('upload_data' => $this->upload->data());
			$imagepath = $data['url']."../uploads/".$image_array['upload_data']['file_name'];
			$this->resizeImage($image_array['upload_data']['full_path'], '70');
			$this->newsletter_model->set_last_curation($nl_id, $imagepath);
			$cu = $this->newsletter_model->get_last_curations($nl_id, null);
			$data['curation'] = $cu[0];
			$this->load->view('templates/header', $data);	
			$this->load->view('nl_builder/create_last_curation_success', $data);
			$this->load->view('templates/footer');
		}

	}


	/**
	* Edit last curation
	*
	* @param int $cu_id the id of the last curation
	*/
	public function edit_last_curation($cu_id){
		$data['subtitle'] = 'Edit Last Curation';
		$cu = $this->newsletter_model->get_last_curations(null, $cu_id);
		$data['curation'] = $cu[0];
		$nl_id = $data['curation']['newsletter'];
		$data['id'] = $cu_id;
		$data['nl_id'] = $nl_id;
		$data['newsletter_item'] = $this->newsletter_model->get_newsletter($nl_id);
		$data['title'] = $data['newsletter_item']['title'];
		$data['date'] = date("d.m.Y", strtotime($data['newsletter_item']['date']));
		$data['url'] = base_url();
	
		$this->form_validation->set_rules('heading', 'Heading', 'required');
		$this->form_validation->set_rules('link', 'Link to Curation', 'required');
		$this->form_validation->set_rules('subhead', 'Subhead', 'required');
		$this->form_validation->set_rules('excerpt', 'Excerpt', 'required');
		$this->form_validation->set_rules('curator_name', 'Curator Name', 'required');
		$this->form_validation->set_rules('curator_link', 'Link to Curator profile', 'required');

		if ($this->form_validation->run() === FALSE &&  ! $this->upload->do_upload())
		{
			$data['error'] = array('error' => $this->upload->display_errors());
			$this->load->view('templates/header', $data);	
			$this->load->view('nl_builder/edit_last_curation', $data);
			$this->load->view('templates/footer');
		}
		else if(!$this->upload->do_upload()){
			$this->newsletter_model->set_last_curation($nl_id, null, $cu_id);
			$cu = $this->newsletter_model->get_last_curations(null, $cu_id);
			$data['curation'] = $cu[0];
			$this->load->view('templates/header', $data);	
			$this->load->view('nl_builder/create_last_curation_success', $data);
			$this->load->view('templates/footer');
		}
		else
		{
			$image_array = array('upload_data' => $this->upload->data());
			$imagepath = $data['url']."../uploads/".$image_array['upload_data']['file_name'];
			$this->resizeImage($image_array['upload_data']['full_path'], '70');
			$this->newsletter_model->set_last_curation($nl_id, $imagepath, $cu_id);
			$cu = $this->newsletter_model->get_last_curations(null, $cu_id);
			$data['curation'] = $cu[0];
			$this->load->view('templates/header', $data);	
			$this->load->view('nl_builder/create_last_curation_success', $data);
			$this->load->view('templates/footer');
		}
	}

	/**
	* Delete a Last Curation
	*
	* @param int $cu_id the id of the last curation
	*/
	public function delete_last_curation($cu_id){

		$data['subtitle'] = 'Delete Last Curation';
		$data['curation'] = $this->newsletter_model->get_last_curations(null, $cu_id);
		$nl_id = $data['curation'][0]['newsletter'];
		$data['nl_id'] = $nl_id;
		$data['newsletter_item'] = $this->newsletter_model->get_newsletter($nl_id);
		$data['title'] = $data['newsletter_item']['title'];
		$data['date'] = date("d.m.Y", strtotime($data['newsletter_item']['date']));
		$data['url'] = base_url();

		if($this->newsletter_model->delete_last_curation($cu_id)){
			$this->load->view('templates/header', $data);	
			$this->load->view('nl_builder/delete_success', $data);
			$this->load->view('templates/footer');
		}
		else{
			$this->load->view('templates/header', $data);	
			$this->load->view('nl_builder/edit_last_curation', $data);
			$this->load->view('templates/footer');
		}
	}

	/**
	* Create an Insight
	*
	* @param int $nl_id the id of the Newsletter linked to the Insight
	*/
	public function create_insight($nl_id){
		$data['subtitle'] = 'Create Insight';
		$data['insights'] = $this->newsletter_model->get_insights($nl_id);
		$data['nl_id'] = $nl_id;
		$data['newsletter_item'] = $this->newsletter_model->get_newsletter($nl_id);
		$data['title'] = $data['newsletter_item']['title'];
		$data['date'] = date("d.m.Y", strtotime($data['newsletter_item']['date']));
		$data['url'] = base_url();

		$this->form_validation->set_rules('category', 'Category', 'required');
		$this->form_validation->set_rules('heading', 'Heading', 'required');
		$this->form_validation->set_rules('link', 'Link to Article', 'required');
		$this->form_validation->set_rules('heading', 'Heading', 'required');
		$this->form_validation->set_rules('excerpt', 'Excerpt', 'required');

		if ($this->form_validation->run() === FALSE)
		{
			$this->load->view('templates/header', $data);	
			$this->load->view('nl_builder/create_insights', $data);
			$this->load->view('templates/footer');
			
		}
		else
		{
			$this->newsletter_model->set_insight($nl_id);
			$data['insights'] = $this->newsletter_model->get_insights($nl_id);
			$this->load->view('templates/header', $data);	
			$this->load->view('nl_builder/create_insights_success', $data);
			$this->load->view('templates/footer');
		}
	
	}


	/**
	* Edit an Insight
	*
	* @param int $in_id the id of the Insight
	*/
	public function edit_insight($in_id){
		$data['subtitle'] = 'Edit Insight';
		$in = $this->newsletter_model->get_insights(null, $in_id);
		$data['insight'] = $in[0];
		$nl_id = $data['insight']['newsletter'];
		$data['id'] = $in_id;
		$data['nl_id'] = $nl_id;
		$data['newsletter_item'] = $this->newsletter_model->get_newsletter($nl_id);
		$data['title'] = $data['newsletter_item']['title'];
		$data['date'] = date("d.m.Y", strtotime($data['newsletter_item']['date']));
		$data['url'] = base_url();
	
		$this->form_validation->set_rules('category', 'Category', 'required');
		$this->form_validation->set_rules('heading', 'Heading', 'required');
		$this->form_validation->set_rules('link', 'Link to Article', 'required');
		$this->form_validation->set_rules('heading', 'Heading', 'required');
		$this->form_validation->set_rules('excerpt', 'Excerpt', 'required');

		if ($this->form_validation->run() === FALSE)
		{
			$data['error'] = array('error' => $this->upload->display_errors());
			$this->load->view('templates/header', $data);	
			$this->load->view('nl_builder/edit_insight', $data);
			$this->load->view('templates/footer');
		}
		else
		{
			$this->newsletter_model->set_insight($nl_id, $in_id);
			$data['insights'] = $this->newsletter_model->get_insights($nl_id);
			$this->load->view('templates/header', $data);	
			$this->load->view('nl_builder/create_insights_success', $data);
			$this->load->view('templates/footer');
		}
	}

	/**
	* Delete an Insight
	*
	* @param int $in_id the id of the insight
	*/
	public function delete_insight($in_id){

		$data['subtitle'] = 'Edit Insight';
		$data['insight'] = $this->newsletter_model->get_insights(null, $in_id);
		$nl_id = $data['insight'][0]['newsletter'];
		$data['nl_id'] = $nl_id;
		$data['newsletter_item'] = $this->newsletter_model->get_newsletter($nl_id);
		$data['title'] = $data['newsletter_item']['title'];
		$data['date'] = date("d.m.Y", strtotime($data['newsletter_item']['date']));
		$data['url'] = base_url();

		if($this->newsletter_model->delete_insight($in_id)){
			$this->load->view('templates/header', $data);	
			$this->load->view('nl_builder/delete_success', $data);
			$this->load->view('templates/footer');
		}
		else{
			$this->load->view('templates/header', $data);	
			$this->load->view('nl_builder/edit_insight', $data);
			$this->load->view('templates/footer');
		}
	}

	/**
	* Create an Artwork
	*
	* @param int $nl_id the id of the Newsletter linked to the Artwork
	*/
	public function create_artwork($nl_id){
		$data['subtitle'] = 'Create Artwork';
		$data['artworks'] = $this->newsletter_model->get_artworks($nl_id, null);
		$data['nl_id'] = $nl_id;
		
		$data['newsletter_item'] = $this->newsletter_model->get_newsletter($nl_id);
		$data['title'] = $data['newsletter_item']['title'];
		$data['date'] = date("d.m.Y", strtotime($data['newsletter_item']['date']));
		$data['url'] = base_url();
		$data['noimage'] = "";
	
		$this->form_validation->set_rules('title', 'Title', 'required');
		$this->form_validation->set_rules('artistname', 'Artist Name', 'required');
		$this->form_validation->set_rules('artistlink', 'Link to Artist Profile', 'required');
		$this->form_validation->set_rules('price', 'Price', 'required');
		$this->form_validation->set_rules('link', 'Link to Artwork', 'required');

		if ($this->form_validation->run() === FALSE ||  ! $this->upload->do_upload())
		{

			$data['error'] = array('error' => $this->upload->display_errors());
			$this->load->view('templates/header', $data);	
			$this->load->view('nl_builder/create_artworks', $data);
			$this->load->view('templates/footer');
			
		}
		else
		{
			$image_array = array('upload_data' => $this->upload->data());
			$imagepath = $data['url']."../uploads/".$image_array['upload_data']['file_name'];
			$this->resizeImage($image_array['upload_data']['full_path'], '180');
			$this->newsletter_model->set_artwork($nl_id, $imagepath);
			$data['artworks'] = $this->newsletter_model->get_artworks($nl_id);
			$this->load->view('templates/header', $data);	
			$this->load->view('nl_builder/create_artworks_success', $data);
			$this->load->view('templates/footer');
		}
	
	}
	
	/**
	* Edit an Artwork
	*
	* @param int $aw_id the id of the Artwork
	*/
	public function edit_artwork($aw_id){
		$data['subtitle'] = 'Edit Artwork';
		$aw = $this->newsletter_model->get_artworks(null, $aw_id);
		$data['artwork'] = $aw[0];
		$nl_id = $data['artwork']['newsletter'];
		$data['id'] = $aw_id;
		$data['nl_id'] = $nl_id;
		$data['newsletter_item'] = $this->newsletter_model->get_newsletter($nl_id);
		$data['title'] = $data['newsletter_item']['title'];
		$data['date'] = date("d.m.Y", strtotime($data['newsletter_item']['date']));
		$data['url'] = base_url();

		$this->form_validation->set_rules('title', 'Title', 'required');
		$this->form_validation->set_rules('artistname', 'Artist Name', 'required');
		$this->form_validation->set_rules('artistlink', 'Link to Artist Profile', 'required');
		$this->form_validation->set_rules('price', 'Price', 'required');
		$this->form_validation->set_rules('link', 'Link to Artwork', 'required');
	

		if ($this->form_validation->run() === FALSE &&  ! $this->upload->do_upload())
		{
			$data['error'] = array('error' => $this->upload->display_errors());
			$this->load->view('templates/header', $data);	
			$this->load->view('nl_builder/edit_artwork', $data);
			$this->load->view('templates/footer');
		}
		else if(!$this->upload->do_upload()){
			$this->newsletter_model->set_artwork($nl_id, null, $aw_id);
			$data['artworks'] = $this->newsletter_model->get_artworks($nl_id);
			$this->load->view('templates/header', $data);	
			$this->load->view('nl_builder/create_artworks_success', $data);
			$this->load->view('templates/footer');
		}
		else
		{
			$image_array = array('upload_data' => $this->upload->data());
			$imagepath = $data['url']."../uploads/".$image_array['upload_data']['file_name'];
			$this->resizeImage($image_array['upload_data']['full_path'], '180');
			$this->newsletter_model->set_artwork($nl_id, $imagepath, $aw_id);
			$data['artworks'] = $this->newsletter_model->get_artworks($nl_id);
			$this->load->view('templates/header', $data);	
			$this->load->view('nl_builder/create_artworks_success', $data);
			$this->load->view('templates/footer');
		}
	}

	/**
	* Delete an Artwork
	*
	* @param int $aw_id the id of the Artwork
	*/
	public function delete_artwork($aw_id){

		$data['subtitle'] = 'Edit Artwork';
		$data['artwork'] = $this->newsletter_model->get_artworks(null, $aw_id);
		$nl_id = $data['artwork'][0]['newsletter'];
		$data['nl_id'] = $nl_id;
		$data['newsletter_item'] = $this->newsletter_model->get_newsletter($nl_id);
		$data['title'] = $data['newsletter_item']['title'];
		$data['date'] = date("d.m.Y", strtotime($data['newsletter_item']['date']));
		$data['url'] = base_url();

		if($this->newsletter_model->delete_artwork($aw_id)){
			$this->load->view('templates/header', $data);	
			$this->load->view('nl_builder/delete_success', $data);
			$this->load->view('templates/footer');
		}
		else{
			$this->load->view('templates/header', $data);	
			$this->load->view('nl_builder/edit_artwork', $data);
			$this->load->view('templates/footer');
		}
	}

	/**
	* Checks if a Newsletter is ready for Export
	* @param int $nl_id the Newsletter's ID
	* @return bool true if newsletter is ready for Export
	*/
	public function is_complete($nl_id){

		$current_cu_size = sizeof($this->newsletter_model->get_current_curations($nl_id));
		$last_cu_size = sizeof($this->newsletter_model->get_last_curations($nl_id));
		$aw_size = sizeof($this->newsletter_model->get_artworks($nl_id));
		$in_size = sizeof($this->newsletter_model->get_insights($nl_id));

		if($current_cu_size > 0 && $last_cu_size > 0 && ($aw_size >= 6 || $aw_size==3) && $in_size > 2){
			return TRUE;
		}
		return FALSE;
	}


	public function export($nl_id){
		$this->load->helper('file');
		$data['url'] = base_url();
		$data['newsletter_item'] = $this->newsletter_model->get_newsletter($nl_id);
		$data['title'] = $data['newsletter_item']['title'];
		$cu_array = $this->newsletter_model->get_current_curations($nl_id);
		$data['current_curation'] = $cu_array[0];
		$l_cu_array = $this->newsletter_model->get_last_curations($nl_id);
		$data['last_curation'] = $l_cu_array[0];
		$data['artworks'] = $this->newsletter_model->get_artworks($nl_id);
		$data['insights'] = $this->newsletter_model->get_insights($nl_id);

		$path= './uploads/newsletter_'.$nl_id.'.html';
		$data['path_global'] = base_url()."../uploads/newsletter_".$nl_id.'.html';
		$string = $this->load->view('nl_builder/export', $data, true);
		// Write the contents back to the file
		//write_file($data['path'], $string );

		if ( ! write_file($path, $string))
		{
		     echo 'Unable to write the file';
		}

		$this->load->view('nl_builder/export', $data);
	}


	public function resizeImage($source, $width){

		$this->load->library('image_lib');
    	$config['source_image'] = $source;
		$config['width'] = $width;
		$config['height'] = '10';
		$config['master_dim'] = 'width';
    	$this->image_lib->initialize($config);

    	if ( ! $this->image_lib->resize())
        {
            echo $this->image_lib->display_errors();
        }

	}
}