<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	http://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There area two reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router what URI segments to use if those provided
| in the URL cannot be matched to a valid route.
|
*/	

$route['newsletter/create'] = 'newsletter/create';
$route['newsletter/create_insight'] = 'newsletter/create_insight';
$route['newsletter/create_current_curation'] = 'newsletter/create_current_curation';
$route['newsletter/create_last_curation'] = 'newsletter/create_last_curation';
$route['newsletter/create_artwork'] = 'newsletter/create_artwork';
$route['newsletter/(:any)/edit_newsletter'] = 'newsletter/edit_newsletter/$1';
$route['newsletter/(:any)/delete_newsletter'] = 'newsletter/delete_newsletter/$1';
$route['newsletter/(:any)/create_insight'] = 'newsletter/create_insight/$1';
$route['newsletter/(:any)/edit_insight'] = 'newsletter/edit_insight/$1';
$route['newsletter/(:any)/delete_insight'] = 'newsletter/delete_insight/$1';
$route['newsletter/(:any)/create_current_curation'] = 'newsletter/create_current_curation/$1';
$route['newsletter/(:any)/edit_current_curation'] = 'newsletter/edit_current_curation/$1';
$route['newsletter/(:any)/delete_current_curation'] = 'newsletter/delete_current_curation/$1';
$route['newsletter/(:any)/create_last_curation'] = 'newsletter/create_last_curation/$1';
$route['newsletter/(:any)/edit_last_curation'] = 'newsletter/edit_last_curation/$1';
$route['newsletter/(:any)/delete_last_curation'] = 'newsletter/delete_last_curation/$1';
$route['newsletter/(:any)/create_artwork'] = 'newsletter/create_artwork/$1';
$route['newsletter/(:any)/edit_artwork'] = 'newsletter/edit_artwork/$1';
$route['newsletter/(:any)/delete_artwork'] = 'newsletter/delete_artwork/$1';
$route['newsletter/(:any)/export'] = 'newsletter/export/$1';
$route['newsletter/(:any)'] = 'newsletter/view/$1';
$route['newsletter/(:any)/current_curation'] = 'current_curation/view/$1';
$route['newsletter/(:any)/last_curation'] = 'last_curation/view/$1';
$route['newsletter/(:any)/insight'] = 'insight/view/$1';

$route['newsletter'] = 'newsletter/index';
//$route['(:any)'] = 'pages/view/$1';
$route['default_controller'] = 'welcome';


/* End of file routes.php */
/* Location: ./application/config/routes.php */