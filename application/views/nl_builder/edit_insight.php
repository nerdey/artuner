<h2><?php echo $subtitle ?></h2>

<table class="fixed artworkRow margin-top bodyContent"  width="600px" cellspacing="">
    <tr>
            <td width="180" align="center" valign="top" class="insights">
                <h7 class="red"><a href="<?php echo $insight['category_link'] ?>"><?php echo $insight['category'] ?></a></h7>
                <h4><a href="<?php echo $insight['link'] ?>"><?php echo $insight['heading'] ?></a></h4>
                <p><?php echo $insight['excerpt'] ?>
                <br/>
                <a href="<?php echo $insight['link'] ?>"  class="read_more">Read more</a></p>
            </td>
            <td width="180" align="center" valign="top" class="insights"> </td>
            <td width="180" align="center" valign="top" class="insights"> </td>
    </tr>
    
</table>

 <div class="border-bottom"></div>

<?php echo validation_errors(); ?>

<?php 
$attributes = array('class' => 'create-form');
echo form_open( $url."newsletter/".$id."/edit_insight", $attributes) ?>
 	<label for="category">Category</label>
	<input type="input" name="category" class="input" value="<?php echo $insight['category'] ?>" placeholder="Category"/>
	<label for="heading">Heading</label>
	<input type="input" name="heading"  class="input" value="<?php echo $insight['heading'] ?>" placeholder="Heading"/>
	<label for="excerpt">Excerpt</label>
	<textarea name="excerpt" class="input" placeholder="Excerpt"/><?php echo $insight['excerpt'] ?></textarea>
	<label for="link">Link to Article</label>
	<input type="input" name="link" class="input" value="<?php echo $insight['link'] ?>" placeholder="Link to Article"/>
	 <div class="border-bottom"></div>
	<input type="submit" name="submit" class="button insight"  id="submit" value="Edit Insight" /> 

</form>
<a href='<?php echo $url?>newsletter/<?php echo $id?>/delete_insight' class="back red" id="btn-overview"> Delete this item </a>
<a href='<?php echo $url?>newsletter/<?php echo $nl_id?>' class="back" id="btn-overview"> Back to Overview </a>
