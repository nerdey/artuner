<h2><?php echo $subtitle ?></h2>
<?php echo validation_errors(); ?>


    <table class="fixed margin-top" width="600px"  >
        <tr>
            <td width="200" align="center">
                    <a href="<?php echo $artwork['link'] ?>"><img src="<?php echo $artwork['image'];?>"></a>
            </td>
            <td width="200" align="center"></td>
            <td width="200" align="center"></td>
        </tr>
        <tr class="subs">
            <td>
               <h7><a href="<?php echo $artwork['link'] ?>"><?php echo $artwork['title']?></a><br/>
                        <a href="<?php echo $artwork['artistlink'] ?>"><?php echo $artwork['artistname']?></a></h7>
                    <h7 class="price"><?php echo $artwork['price']?></h7>
            </td>
        </tr>
    </table>

    <div class="border-bottom"></div>
	<br/>

<?php 
$attributes = array('class' => 'create-form');
echo form_open_multipart($url."newsletter/".$id."/edit_artwork", $attributes) ?>
    <label for="title">Title</label>
	<input type="input" name="title" class="input" value="<?php echo $artwork['title']; ?>" placeholder="Title"/>
	<label for="artistname">Name of Artist</label>
    <input type="input" name="artistname" class="input" value="<?php echo $artwork['artistname']; ?>" placeholder="Name of Artist"/>
	<label for="artistlink">Link to Artist on Artuner.com</label>
    <input type="input" name="artistlink"  class="input" value="<?php echo $artwork['artistlink'] ?>" placeholder="Link to Artist"/>
	<label for="price">Price</label>
    <input type="input" name="price" class="input" value="<?php echo $artwork['price'] ?>" placeholder="Price"/>
	<label for="link">Link to Artwork on Artuner.com</label>
    <input type="input" name="link" class="input" value="<?php echo $artwork['link'] ?>" placeholder="Link to Artwork"/>
    <label for="userfile">Image of Artwork</label>
	<div id="filepath" class="input">Image</div>
	<div id="filebutton" class="button">Upload</div>
	<input type="file" name="userfile" value="<?php echo set_value('userfile'); ?>" id="file-input" class="margin"/>
    <div class="border-bottom"></div>
	<input type="submit" name="submit" class="button" id="submit" value="Edit Artwork" /> 
</form>
<a href='<?php echo $url?>newsletter/<?php echo $id?>/delete_artwork' class="back red" id="btn-overview"> Delete this item </a>
<a href='<?php echo $url?>newsletter/<?php echo $nl_id?>' class="back" id="btn-overview"> Back to Overview </a>

