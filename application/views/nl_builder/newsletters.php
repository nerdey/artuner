
<?php echo validation_errors(); ?>

<?php 
$attributes = array('class' => 'newsletters');
echo form_open(''.$url.'newsletter/create', $attributes) ?>
	<label for="title">Title</label>
	<input type="input" name="title" class="input margin" value="" placeholder="Title"/>
	<label for="message">Welcome Message</label>
	<textarea name="message" class="input" placeholder="Welcome Message" ></textarea>
	 <div class="border-bottom"></div>
	<input type="submit" name="submit" class="button" value="Create newsletter" /> 
</form>
<br/>
<ul class="newsletter-list">
<?php foreach ($newsletters as $newsletter_item): ?>
<li>
    <h4 class="date left"><?php echo date("d.m.Y", strtotime($newsletter_item['date']))?></h4>
    <h2 class=""><?php echo $newsletter_item['title'] ?></h2>
    <h7 class="red"><a href="<?php echo $url.'newsletter/'.$newsletter_item['id'].'/edit_newsletter'?>">EDIT</a></h7>
    
    <a class="button" href="<?php echo $url.'newsletter/'.$newsletter_item['id'] ?>">Edit Contents</a></p>
</li>
<?php endforeach ?>

