
<div id="sub-header" <?php if(!$is_complete):?>class="incomplete"<?php endif;?>>

<p class="message"><?php if(!$is_complete):?>The Newsletter in not complete yet. You have to create all components before you can Export the code.<?php else:?>The Newsletter is complete. If there‘s nothing more to edit you can export the Newsletter.<?php endif;?></p>
<a class="button <?php if(!$is_complete):?>inactive<?php endif;?>" <?php if($is_complete === TRUE){ echo 'href='.$url.'newsletter/'.$id.'/export' ;}?> target="_blank">Export Newsletter</a>
</div>
<div id="content">
<table border="0" cellpadding="0" cellspacing="0" width="580" id="templatePreheader" style="padding: 20px 0 0 0;background-color: #FFFFFF;">
    <tr valign="top">
                    <td style="border-collapse: collapse;">
                        <img src="<?php echo $url ?>../assets/css/img/logo_newsletter.png" alt="Artuner" id="headerLogo" style="border: 0;height: auto;line-height: 100%;outline: none;text-decoration: none;margin-bottom: 20px;"><br> 
                    </td>
                    <td style="border-collapse: collapse;">
                        <p class="preheader" style="font-size: 10px;text-align: right;margin-top: 0px;">Problems with reading this email? <a href="" class="underline" style="color: #232323;">View online.</a></p>
                    </td>
                </tr>
</table>
                        <table border="0" cellpadding="0" cellspacing="0" width="580" id="templateContainer" style="padding: 0 1px 0 1px;border: none;background-color: #FFFFFF;">
                                        
    <tr class="navi">
        <td class="first" style="border-collapse: collapse;width: 140px;"><a href="http://www.artuner.com" style="color: #232323;"><img src="<?php echo $url ?>../assets/css/img/nav_artuner_now.png" alt="artuner now" style="border: 0;height: auto;line-height: 100%;outline: none;text-decoration: none;"></a></td>
        <td style="border-collapse: collapse;width: 90px;"><a href="http://www.artuner.com/all-art/" style="color: #232323;"><img src="<?php echo $url ?>../assets/css/img/nav_all_art.png" alt="all art" style="border: 0;height: auto;line-height: 100%;outline: none;text-decoration: none;"></a></td>
        <td style="border-collapse: collapse;width: 90px;"><a href="http://www.artuner.com/artists/" style="color: #232323;"><img src="<?php echo $url ?>../assets/css/img/nav_artists.png" alt="artists" style="border: 0;height: auto;line-height: 100%;outline: none;text-decoration: none;"></a></td>
        <td style="border-collapse: collapse;width: 90px;"><a href="http://www.artuner.com/experts/" style="color: #232323;"><img src="<?php echo $url ?>../assets/css/img/nav_experts.png" alt="experts" style="border: 0;height: auto;line-height: 100%;outline: none;text-decoration: none;"></a></td>
        <td style="border-collapse: collapse;"><a href="http://www.artuner.com/insights/" style="color: #232323;"><img src="<?php echo $url ?>../assets/css/img/nav_insights.png" alt="insights" style="border: 0;height: auto;line-height: 100%;outline: none;text-decoration: none;"></a></td>
    </tr>

</table>
<table>
    <tr>
         <td valign="top" width="580" class="contentRow" style="border-collapse: collapse;  padding: 40px 0 20px 0px;border-bottom: 1px solid #ededed;">
             <h4 class="curation-heading" style="color: #232323;display: block;text-transform: none;font-family: &quot;Georgia&quot;, serif;font-size: 22px;font-weight: normal; font-style: italic;line-height: 130%;text-align: left;padding-right: 20px;margin-top: 0;margin-right: 0;margin-bottom: 10px;margin-left: 0;">
                   <?php if(isset($newsletter_item['message'])){echo $newsletter_item['message'];}?>
             </h4>
             <h7 class="red"><a href="<?php echo $url.'newsletter/'.$id.'/edit_newsletter'?>">EDIT</a></h7>
         </td>
    </tr>
</table>

<?php 
if($current_curations){
?>

<?php
foreach ($current_curations as $curation): ?>

 <table class="bodyContent margin-top">
    <tr>
        <td valign="top" width="350">
            <h4 style="padding-right: 20px;"><a href="<?php echo $curation['link'] ?>">Current Curation /<br/>
            <span class="red"><?php echo $curation['heading'] ?></span></a></h4>
            <h5><?php echo $curation['subhead'] ?></h5>
            <p>
                <?php echo $curation['excerpt'] ?> <br/>
                <a href="<?php echo $curation['link'] ?>"  class="read_more">Read More</a>
                <h7 class="red"><a href="<?php echo $url.'newsletter/'.$curation['id'].'/edit_current_curation'?>">EDIT</a></h7>
            </p>
        </td>
 
         <td valign="top"  width="140">
            <div class="border_right curatedBy">
                <h7>curated by:</h7>
                <h6><a href="<?php echo $curation['curator_link'] ?>"><?php echo $curation['curator_name'] ?></a></h6> 
            </div>   
         </td>
         <td valign="top" class=" curator" >
            <a href="<?php echo $curation['curator_link'] ?>" style="display:block"><img src="<?php echo $curation['curator_image'] ?>"></a>  
         </td>
    </tr>
</table>

<?php endforeach; }
else{ ?>
<a class="create" href='<?php echo $id ?>/create_current_curation/'>Create Current Curation</a>
<?php }
?>

<?php 
if($artworks){
    $size = sizeof($artworks);
    ?>
    <table class="fixed margin-top" width="600px" >
        <tr>

        <?php for($i= 0; $i <= 2; $i++){ ?>
            <td width="200" align="center">
                <?php if($i<$size): ?>
                <a href="<?php echo $artworks[$i]['link'] ?>"><img src="<?php echo $artworks[$i]['image'];?>"></a>
                <?php endif; ?>
            </td>
        <?php 
        }
        ?>
        </tr>
        <tr class="subs">

        <?php for($i= 0; $i < $size && $i <= 2; $i++){ ?>
            <td>
                <?php if($i<$size){?>
                <h7><a href="<?php echo $artworks[$i]['link'] ?>"><?php echo $artworks[$i]['title']?></a><br/>
                    <a href="<?php echo $artworks[$i]['artistlink'] ?>"><?php echo $artworks[$i]['artistname']?></a></h7>
                <h7 class="price"><?php echo $artworks[$i]['price']?></h7>
                <h7 class="red"><a href="<?php echo $url.'newsletter/'.$artworks[$i]['id'].'/edit_artwork'?>">EDIT</a></h7>
                <?php }?>
            </td>
        <?php 
        }
        ?>
        </tr>
    </table>

    <?php if($size > 3): ?>
        <table class="fixed artworkRow margin-top" width="600px" >
            <tr>
            <?php for($i= 3; $i <= 5; $i++){ ?>
                <td width="200" align="center">
                        <?php if($i<$size): ?>
                        <a href="<?php echo $artworks[$i]['link'] ?>"><img src="<?php echo $artworks[$i]['image'];?>"></a>
                        <?php endif; ?>
                </td>
            <?php 
            }
            ?>

            </tr>
            <tr class="subs">

             <?php for($i= 3; $i < $size && $i <= 5; $i++){ ?>
                <td>
                <?php if($i<$size){?>
                    <h7><a href="<?php echo $artworks[$i]['link'] ?>"><?php echo $artworks[$i]['title']?></a><br/>
                        <a href="<?php echo $artworks[$i]['artistlink'] ?>"><?php echo $artworks[$i]['artistname']?></a></h7>
                    <h7 class="price"><?php echo $artworks[$i]['price']?></h7>
                    <h7 class="red"><a href="<?php echo $url.'newsletter/'.$artworks[$i]['id'].'/edit_artwork'?>">EDIT</a></h7>
                <?php }?>
                </td>
            <?php 
            }
            ?>
            </tr>
        </table>
    <?php
    endif;
    ?>
    <table class="fixed artworkRow margin-top" width="600px" >
        <tr >
            <td></td>
            <td align="center" height="50">
                <a href="<?php if($current_curations){echo $current_curations[0]['link']; }?>">
                    <img src="<?php echo $url?>../assets/css/img/button.jpg">
                </a>
            </td>
            <td></td>
        </tr>
</table>
        <?php
        if($size < 6){?>
            <a class="create add" href='<?php echo $id ?>/create_artwork/'>Add Artworks</a>
  <?php }
    }
else{ ?>
<a class="create" href='<?php echo $id ?>/create_artwork/'>Create Artworks</a>
<?php }
?>

<?php 
if($last_curations){
?>

<?php
foreach ($last_curations as $curation): ?>

 <table class="bodyContent margin-top">
    <tr>
        <td valign="top" width="350">
            <h4 style="padding-right: 20px;"><a href="<?php echo $curation['link'] ?>">Last Curation /<br/>
            <span class="red"><?php echo $curation['heading'] ?></span></a></h4>
            <h5><?php echo $curation['subhead'] ?></h5>
            <p>
                <?php echo $curation['excerpt'] ?> <br/>
                <a href="<?php echo $curation['link'] ?>"  class="read_more">Read More</a>
                <h7 class="red"><a href="<?php echo $url.'newsletter/'.$curation['id'].'/edit_last_curation'?>">EDIT</a></h7>
            </p>
        </td>
 
         <td valign="top"  width="140" >
            <div class="border_right curatedBy">
                <h7>curated by:</h7>
                <h6><a href="<?php echo $curation['curator_link'] ?>"><?php echo $curation['curator_name'] ?></a></h6> 
            </div>   
         </td>
         <td valign="top" class=" curator" >
            <a href="<?php echo $curation['curator_link'] ?>" style="display:block"><img src="<?php echo $curation['curator_image'] ?>"></a>  
         </td>
    </tr>
</table>

<?php endforeach; }
else{ ?>
<a class="create" href='<?php echo $id ?>/create_last_curation/'>Create Last Curation</a>
<?php }
?>


<?php
if($insights){
    $size = sizeof($insights);
    ?>

<table class="fixed artworkRow margin-top bodyContent"  width="600px" cellspacing="">
    <tr valign="top">
    <?php for($i= 0; $i <= 2; $i++){ ?>
            <td width="180" align="center" valign="top" class="insights">
                <?php if($i<$size):?>
                <h7 class="red"><?php echo $insights[$i]['category'] ?></h7>
                <h4><a href="<?php echo $insights[$i]['link'] ?>"><?php echo $insights[$i]['heading'] ?></a></h4>
                <p><?php echo $insights[$i]['excerpt'] ?>
                <br/>
                <a href="<?php echo $insights[$i]['link'] ?>"  class="read_more">Read More</a></p>
                 <h7 class="red"><a href="<?php echo $url.'newsletter/'.$insights[$i]['id'].'/edit_insight'?>">EDIT</a></h7>
                <?php endif;?>
            </td>
        <?php 
        }
        ?>
    </tr>
    
</table>

<?php   if($size < 3){?>
            <a class="create add" href='<?php echo $id ?>/create_insight/'>Add Insights</a>
  <?php }
}
else{ ?>
<a class="create" href='<?php echo $id ?>/create_insight/'>Create Insights</a>
<?php }
?>

<table border="0" cellpadding="0" cellspacing="0" width="600" id="templateFooter">
        		<tr>
                	<td align="center" valign="top" class="socialMedia">
                		<img id="stayTuned" src="http://www.artuner.com/nlbuilder/assets/css/img/newsletter_stay.jpg"><br/>
                		<a href="http://www.twitter.com/theartuner"><img src="http://www.artuner.com/nlbuilder/assets/css/img/twitter.png"></a>
                		<a href="http://facebook.com/theartuner"><img src="http://www.artuner.com/nlbuilder/assets/css/img/fb.png"></a>
                		<a href="http://theartuner.tumblr.com/"><img src="http://www.artuner.com/nlbuilder/assets/css/img/tumblr.png"></a>
                    	<a href="http://pinterest.com/theartuner/"><img src="http://www.artuner.com/nlbuilder/assets/css/img/pin.png"></a>
                       
                    </td>
                </tr>
            	<tr>
                	<td valign="top" class="footerContent" style="color:#232323; font-family:georgia; font-size:10px; text-align: center;">
                    
                       <p>If you no longer wish to receive e-mails from Artuner, <a href="http://www.artuner.com/unsubscribe.html" class="underline">please unsubscribe here.</a><br/>
                       	Artuner Ltd., 36 Alie Street, London, UK, E1 8DA.<br/>
                    </td>
                </tr>
            </table>

<a href='<?php echo $url?>newsletter/' class="back">Back to All Newsletters</a>
