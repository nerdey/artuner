<h2><?php echo $subtitle ?></h2>

<h2>The current curation has successfully been created:</h2>
<div class="border-bottom"></div>
<?php 

?>



 <table class="bodyContent margin-top">
    <tr>
        <td valign="top" width="300">
            <h4>Current Curation /<br/>
            <span class="red"><?php echo $curation['heading'] ?></span></h4>
            <h5><?php echo $curation['subhead'] ?></h5>
            <p>
                <?php echo $curation['excerpt'] ?> <br/>
                <a href="<?php echo $curation['link'] ?>"  class="read_more">Read more</a>
                <h7 class="red"><a href="<?php echo $url.'newsletter/'.$curation['id'].'/edit_current_curation'?>">EDIT</a></h7>
            </p>
        </td>
 
         <td valign="top"  width="200">
            <div class="border_right curatedBy">
                <h7>curated by:</h7>
                <h6><a href="<?php echo $curation['curator_link'] ?>"><?php echo $curation['curator_name'] ?></a></h6> 
            </div>   
         </td>
         <td valign="top" class=" curator" >
            <img src="<?php echo $curation['curator_image'] ?>">  
         </td>
    </tr>
</table>

<div class="border-bottom"></div>
<a href='<?php echo $url?>newsletter/<?php echo $nl_id?>' class="back" id="btn-overview"> Back to Overview </a>
