<h2><?php echo $subtitle ?></h2>
<?php echo validation_errors(); 
?>

<?php 
$attributes = array('class' => 'create-form');
echo form_open_multipart($url."newsletter/".$nl_id."/create_artwork", $attributes) ?>
	<label for="title">Title</label>
	<input type="input" name="title" class="input" value="<?php echo set_value('title'); ?>" placeholder="Title"/>
	<label for="artistname">Name of Artist</label>
	<input type="input" name="artistname" class="input" value="<?php echo set_value('artistname'); ?>" placeholder="Name of Artist"/>
	<label for="artistlink">Link to Artistpage on Artuner.com</label>
	<input type="input" name="artistlink"  class="input" value="<?php echo set_value('artistlink'); ?>" placeholder="Link to Artistpage"/>
	<label for="price">Price</label>
	<input type="input" name="price" class="input" value="<?php echo set_value('price'); ?>" placeholder="Price"/>
	<label for="link">Link to Artwork on Artuner.com</label>
	<input type="input" name="link" class="input" value="<?php echo set_value('link'); ?>" placeholder="Link to Artwork"/>
	<label for="userfile">Image of Artwork</label>
	<div id="filepath" class="input">Image</div>
	<div id="filebutton" class="button">Upload</div>
	<input type="file" name="userfile" value="<?php echo set_value('userfile'); ?>" id="file-input" class="margin"/>
	<div class="border-bottom"></div>
	<input type="submit" name="submit" class="button" id="submit" value="Create Artwork" /> 
</form>
<a href='<?php echo $url?>newsletter/<?php echo $nl_id?>' class="back" id="btn-overview"> Back to Overview </a>
