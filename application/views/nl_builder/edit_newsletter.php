<h2><?php echo $subtitle ?></h2>
<?php echo validation_errors(); ?>

<table border="0" cellpadding="0" cellspacing="0" width="580" id="templatePreheader" style="padding: 20px 0 0 0;background-color: #FFFFFF;">
    <tr valign="top">
                    <td style="border-collapse: collapse;">
                        <img src="<?php echo $url ?>../assets/css/img/logo_newsletter.png" alt="Artuner" id="headerLogo" style="border: 0;height: auto;line-height: 100%;outline: none;text-decoration: none;margin-bottom: 20px;"><br> 
                    </td>
                    <td style="border-collapse: collapse;">
                        <p class="preheader" style="font-size: 10px;text-align: right;margin-top: 0px;">Problems with reading this email? <a href="" class="underline" style="color: #232323;">View online.</a></p>
                    </td>
                </tr>
</table>
                        <table border="0" cellpadding="0" cellspacing="0" width="580" id="templateContainer" style="padding: 0 1px 0 1px;border: none;background-color: #FFFFFF;">
                                        
    <tr class="navi">
        <td class="first" style="border-collapse: collapse;width: 200px;"><a href="" style="color: #232323;"><img src="<?php echo $url ?>../assets/css/img/nav_artuner_now.jpg" alt="artuner now" style="border: 0;height: auto;line-height: 100%;outline: none;text-decoration: none;"></a></td>
        <td style="border-collapse: collapse;"><a href="" style="color: #232323;"><img src="<?php echo $url ?>../assets/css/img/nav_all_art.jpg" alt="all art" style="border: 0;height: auto;line-height: 100%;outline: none;text-decoration: none;"></a></td>
        <td style="border-collapse: collapse;"><a href="" style="color: #232323;"><img src="<?php echo $url ?>../assets/css/img/nav_artists.jpg" alt="artists" style="border: 0;height: auto;line-height: 100%;outline: none;text-decoration: none;"></a></td>
        <td style="border-collapse: collapse;"><a href="" style="color: #232323;"><img src="<?php echo $url ?>../assets/css/img/nav_experts.jpg" alt="experts" style="border: 0;height: auto;line-height: 100%;outline: none;text-decoration: none;"></a></td>
        <td style="border-collapse: collapse;"><a href="" style="color: #232323;"><img src="<?php echo $url ?>../assets/css/img/nav_partner.jpg" alt="partner" style="border: 0;height: auto;line-height: 100%;outline: none;text-decoration: none;"></a></td>
        <td style="border-collapse: collapse;"><a href="" style="color: #232323;"><img src="<?php echo $url ?>../assets/css/img/nav_insights.jpg" alt="insights" style="border: 0;height: auto;line-height: 100%;outline: none;text-decoration: none;"></a></td>
    </tr>

</table>
<table>
    <tr>
         <td valign="top" width="580" class="contentRow" style="border-collapse: collapse;  padding: 40px 0 20px 0px;border-bottom: 1px solid #ededed;">
             <h4 class="curation-heading" style="color: #232323;display: block;text-transform: none;font-family: &quot;Georgia&quot;, serif;font-size: 22px;font-weight: normal; font-style: italic;line-height: 130%;text-align: left;padding-right: 20px;margin-top: 0;margin-right: 0;margin-bottom: 10px;margin-left: 0;">
                 <?php if(isset($newsletter_item['message'])){echo $newsletter_item['message'];}?>
             </h4>
         </td>
    </tr>
</table>
<div class="border-bottom"></div>
<?php 
$attributes = array('class' => 'newsletters');
echo form_open(''.$url.'newsletter/'.$nl_id.'/edit_newsletter', $attributes) ?>
 	<label for="title">Title</label>
	<input type="input" name="title" class="input margin" value="<?php echo $newsletter_item['title']; ?>" placeholder="Title"/>
	<label for="message">Welcome Message</label>
	<textarea name="message" class="input"><?php echo $newsletter_item['message']; ?></textarea>
	<div class="border-bottom"></div>
	<input type="submit" name="submit" class="button" value="Edit newsletter" /> 
</form>


<a href='<?php echo $url?>newsletter/<?php echo $nl_id?>/delete_newsletter' class="back red" id="btn-overview"> Delete this item </a>
<a href='<?php echo $url?>newsletter/<?php echo $nl_id?>' class="back" id="btn-overview"> Back to Overview </a>