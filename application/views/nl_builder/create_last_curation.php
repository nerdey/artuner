<h2><?php echo $subtitle ?></h2>

<?php echo validation_errors(); ?>

<?php 
$attributes = array('class' => 'create-form');
echo form_open_multipart($url."newsletter/".$nl_id."/create_last_curation", $attributes) ?>
	<label for="heading">Heading</label>
	<input type="input" name="heading" class="input" value="<?php echo set_value('heading'); ?>" placeholder="Heading"/>
	<label for="subhead">Subhead</label>
	<input type="input" name="subhead" class="input" value="<?php echo set_value('subhead'); ?>" placeholder="Subhead"/>
	<label for="excerpt">Excerpt</label>
	<textarea name="excerpt" class="input" placeholder="Excerpt"/><?php echo set_value('excerpt'); ?></textarea>
	<label for="link">Link to Last Curation on Artuner.com</label>
	<input type="input" name="link"  class="input" value="<?php echo set_value('link'); ?>" placeholder="Link to Last Curation"/>
	<label for="curator_name">Name of Curator</label>
	<input type="input" name="curator_name" class="input" value="<?php echo set_value('curator_name'); ?>" placeholder="Name of Curator"/>
	<label for="curator_link">Link to Curator Page on Artuner.com</label>
	<input type="input" name="curator_link" class="input" value="<?php echo set_value('curator_link'); ?>" placeholder="Link to Curator"/>
	<label for="userfile">Curator Image</label>
	<div id="filepath" class="input">Curator Image</div>
	<div id="filebutton" class="button">Upload</div>
	<input type="file" name="userfile" id="file-input" value="<?php echo set_value('userfile'); ?>" class="margin"/>
	<div class="border-bottom"></div>
	<input type="submit" name="submit" class="button" id="submit" value="Create Last Curation" /> 

</form>
<a href='<?php echo $url?>newsletter/<?php echo $nl_id?>' class="back" id="btn-overview"> Back to Overview </a>
