<h2><?php echo $subtitle ?></h2>
<?php echo validation_errors(); ?>

 <table class="bodyContent margin-top">
    <tr>
        <td valign="top" width="300">
            <h4>Current Curation /<br/>
            <span class="red"><?php echo $curation['heading'] ?></span></h4>
            <h5><?php echo $curation['subhead'] ?></h5>
            <p>
                <?php echo $curation['excerpt'] ?> <br/>
                <a href="<?php echo $curation['link'] ?>"  class="read_more">Read more</a>
            </p>
        </td>
 
         <td valign="top"  width="200">
            <div class="border_right curatedBy">
                <h7>curated by:</h7>
                <h6><a href="<?php echo $curation['curator_link'] ?>"><?php echo $curation['curator_name'] ?></a></h6> 
            </div>   
         </td>
         <td valign="top" class=" curator" >
            <img src="<?php echo $curation['curator_image'] ?>">  
         </td>
    </tr>
</table>

 <div class="border-bottom"></div>

<?php 
$attributes = array('class' => 'create-form');
echo form_open_multipart($url."newsletter/".$id."/edit_current_curation", $attributes) ?>
    <label for="heading">Heading</label>
    <input type="input" name="heading" class="input" value="<?php echo $curation['heading'] ?>" placeholder="Heading"/>
    <label for="subhead">Subhead</label>
    <input type="input" name="subhead" class="input" value="<?php echo $curation['subhead'] ?>" placeholder="Subhead"/>
    <label for="excerpt">Excerpt</label>
    <textarea name="excerpt" class="input" placeholder="Excerpt"/><?php echo $curation['excerpt'] ?></textarea>
    <label for="link">Link to Current Curation on Artuner.com</label>
    <input type="input" name="link"  class="input" value="<?php echo $curation['link'] ?>" placeholder="Link to Current Curation"/>
    <label for="curator_name">Name of Curator</label>
    <input type="input" name="curator_name" class="input" value="<?php echo $curation['curator_name'] ?>" placeholder="Name of Curator"/>
    <label for="curator_link">Link to Curator Page on Artuner.com</label>
    <input type="input" name="curator_link" class="input" value="<?php echo $curation['curator_link'] ?>" placeholder="Link to Curator"/>
    <label for="userfile">Curator Image</label>
    <div id="filepath" class="input">Curator Image</div>
    <div id="filebutton" class="button">Upload</div>
    <input type="file" name="userfile" id="file-input" value="<?php echo set_value('"userfile'); ?>" class="margin"/>
    <div class="border-bottom"></div>
    <input type="submit" name="submit" class="button" id="submit" value="Edit Current Curation" /> 


</form>
<a href='<?php echo $url?>newsletter/<?php echo $id?>/delete_current_curation' class="back red" id="btn-overview"> Delete this item </a>
<a href='<?php echo $url?>newsletter/<?php echo $nl_id?>' class="back" id="btn-overview"> Back to Overview </a>

