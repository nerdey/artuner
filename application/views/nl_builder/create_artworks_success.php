<h2><?php echo $subtitle ?></h2>

<h2>The artwork has successfully been created. </h2>
<h6>All artworks in this Newsletter:</h6>
<div class="border-bottom"></div>

<?php 
if($artworks){
    $size = sizeof($artworks);
    ?>
    <table class="fixed margin-top" width="600px" >
        <tr>

        <?php for($i= 0; $i <= 2; $i++){ ?>
            <td width="200" align="center">
                    <?php if($i<$size): ?>
                        <a href="<?php echo $artworks[$i]['link'] ?>"><img src="<?php echo $artworks[$i]['image'];?>"></a>
                    <?php endif; ?>
            </td>
        <?php 
        }
        ?>
        </tr>
        <tr class="subs">

        <?php for($i= 0; $i < $size && $i <= 2; $i++){ ?>
            <td>
                <?php if($i<$size){?>
                    <h7><a href="<?php echo $artworks[$i]['link'] ?>"><?php echo $artworks[$i]['title']?></a><br/>
                        <a href="<?php echo $artworks[$i]['artistlink'] ?>"><?php echo $artworks[$i]['artistname']?></a></h7>
                    <h7 class="price"><?php echo $artworks[$i]['price']?></h7>
                    <h7 class="red"><a href="<?php echo $url.'newsletter/'.$artworks[$i]['id'].'/edit_artwork'?>">EDIT</a></h7>
                <?php }?>
            </td>
        <?php 
        }
        ?>
        </tr>
    </table>

    <?php if($size > 3): ?>
        <table class="fixed artworkRow margin-top" width="600px" >
            <tr>
            <?php for($i= 3; $i <= 5; $i++){ ?>
                <td width="200" align="center">
                        <?php if($i<$size): ?>
                        <a href="<?php echo $artworks[$i]['link'] ?>"><img src="<?php echo $artworks[$i]['image'];?>"></a>
                    <?php endif; ?>
                </td>
            <?php 
            }
            ?>

            </tr>
            <tr class="subs">

             <?php for($i= 3; $i < $size && $i <= 5; $i++){ ?>
                <td>
                 <?php if($i<$size){?>
                    <h7><a href="<?php echo $artworks[$i]['link'] ?>"><?php echo $artworks[$i]['title']?></a><br/>
                        <a href="<?php echo $artworks[$i]['artistlink'] ?>"><?php echo $artworks[$i]['artistname']?></a></h7>
                    <h7 class="price"><?php echo $artworks[$i]['price']?></h7>
                    <h7 class="red"><a href="<?php echo $url.'newsletter/'.$artworks[$i]['id'].'/edit_artwork'?>">EDIT</a></h7>
                <?php }?>
                </td>
            <?php 
            }
            ?>
            </tr>
        </table>
        
    <?php
    endif;

    echo "<div class='border-bottom'></div>";

	if ($size < 6){?>
		<a class= "create add" href="<?php echo $nl_id ?>/create_artwork/">Add more Artworks</a>
	<?php 
	}
	else{ 
	?>
		<p>You cannot add more that 6 Artworks to the Newsletter.<br/> You can edit or delete Artworks instead.</p>
	<?php 
	}
}
?>


<a href='<?php echo $url?>newsletter/<?php echo $nl_id?>' class="back" id="btn-overview"> Back to Overview </a>
