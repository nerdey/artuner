<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <meta name="format-detection" content="telephone=no" />
        <!-- Facebook sharing information tags -->
        <meta property="og:title" content="Welcome to Artuner.com">
        <style type="text/css">
        a{
            color:#232323;
            
        }

    
        </style>
        <title>Welcome to Artuner.com</title>
		
	</head>
    <body leftmargin="0" marginwidth="0" topmargin="0" marginheight="0" offset="0" style="-webkit-text-size-adjust: none;margin: 0;padding: 0;background-color: #FFFF;width: 100%;">
    	<center>
        	<table border="0" cellpadding="0" cellspacing="0" height="100%" width="100%" id="backgroundTable" style="padding: 0 10px 0 10px;margin: 0;background-color: #FFFF;height: 100%;width: 100%;">
            	<tr>
                	<td align="center" valign="top" style="border-collapse: collapse;">
                        <!-- // Begin Template Preheader \ -->
                        <table border="0" cellpadding="0" cellspacing="0" width="580" id="templatePreheader" style="padding: 20px 0 0 0;background-color: #FFFFFF;">
                            <tr valign="top">
                                <td style="border-collapse: collapse;">
                                    <img src="<?php echo $url ?>../assets/css/img/logo_newsletter.png" alt="Artuner" id="headerLogo" style="border: 0;height: auto;line-height: 100%;outline: none;text-decoration: none;margin-bottom: 20px;"><br> 
                                </td>
                                <td style="border-collapse: collapse;">
                                    <p class="preheader" style="font-size: 10px;text-align: right;margin-top: 0px;">Problems with reading this email? <a href="<?php echo $path_global;?>" style="color: #232323;">View online.</a></p>
                                </td>
                            </tr>
                        </table>
                        <!-- // End Template Preheader \ -->
                    	<table border="0" cellpadding="0" cellspacing="0" width="580" id="templateContainer" style="padding: 0 1px 0 5px;border: none;background-color: #FFFFFF;">
                        	<tr>
                            	<td align="center" valign="top" style="border-collapse: collapse;">
                                    <!-- // Begin Template Header \ -->
                                	<table border="0" cellpadding="0" cellspacing="0" width="580" id="templateHeader" style="padding: 0 1px 0 0px;background-color: #FFFFFF;border-bottom: 0;table-layout: fixed;">
                                        
                                        <tr class="navi">
                                            <td class="first" style="border-collapse: collapse;width: 140px;"><a href="" style="color: #232323;"><img src="<?php echo $url ?>../assets/css/img/nav_artuner_now.jpg" alt="artuner now" style="border: 0;height: auto;line-height: 100%;outline: none;text-decoration: none;"></a></td>
                                            <td style="border-collapse: collapse;"><a href="" style="color: #232323;"><img src="<?php echo $url ?>../assets/css/img/nav_all_art.jpg" alt="all art" style="border: 0;height: auto;line-height: 100%;outline: none;text-decoration: none;"></a></td>
                                            <td style="border-collapse: collapse;"><a href="" style="color: #232323;"><img src="<?php echo $url ?>../assets/css/img/nav_artists.jpg" alt="artists" style="border: 0;height: auto;line-height: 100%;outline: none;text-decoration: none;"></a></td>
                                            <td style="border-collapse: collapse;"><a href="" style="color: #232323;"><img src="<?php echo $url ?>../assets/css/img/nav_experts.jpg" alt="experts" style="border: 0;height: auto;line-height: 100%;outline: none;text-decoration: none;"></a></td>
                                            <td style="border-collapse: collapse;"><a href="" style="color: #232323;"><img src="<?php echo $url ?>../assets/css/img/nav_partner.jpg" alt="partner" style="border: 0;height: auto;line-height: 100%;outline: none;text-decoration: none;"></a></td>
                                            <td style="border-collapse: collapse;"><a href="" style="color: #232323;"><img src="<?php echo $url ?>../assets/css/img/nav_insights.jpg" alt="insights" style="border: 0;height: auto;line-height: 100%;outline: none;text-decoration: none;"></a></td>
                                        </tr>
                                    </table>
                                    <!-- // End Template Header \ -->
                                </td>
                            </tr>

                        	<tr>
                            	<td align="center" valign="top" style="border-collapse: collapse;">
                                    <!-- // Begin Template Body \ -->
                                	<table border="0" cellpadding="0" cellspacing="0" width="580" id="templateBody" style="padding: 0 1px 0 1px;">
                                    	<tr>
                                        	<td valign="top" width="580" style="border-collapse: collapse;">
                                            	<table border="0" cellpadding="0" cellspacing="0" width="580" style="padding: 0 1px 0 1px;">
                                                	<tr>
                                                    	<td valign="top" style="border-collapse: collapse;">
                                            				<table border="0" cellpadding="0" cellspacing="0" width="580" style="padding: 0 1px 0 1px;">
                                                            	<tr>
                                                                	<td valign="top" class="bodyContent" style="border-collapse: collapse;background-color: #FFFFFF;">
                                                            
                                                                        <!-- // Begin Module: Standard Content \ -->
                                                                        <table border="0" cellpadding="0" cellspacing="0" width="100%" style="padding: 0 1px 0 1px;">
                                                                            <tr>   
                                                                            	<tr>
                                                                                         <td valign="top" width="580" class="contentRow" style="border-collapse: collapse;padding: 40px 0 20px 0px;border-bottom: 1px solid #ededed;">
                                                                                             <h4 class="curation-heading" style="color: #232323;display: block;font-family: &quot;Georgia&quot;, serif;font-size: 22px;font-weight: normal; font-style: italic;line-height: 130%;text-align: left;padding-right: 20px;margin-top: 0;margin-right: 0;margin-bottom: 10px;margin-left: 0;">
                                                                                             	Dear [FIRSTNAME] [LASTNAME], this is a welcome sentence which we will change soon. Enjoy our newsletter.</h4>
                                                                                         </td>
                                                                            	</tr>
                                                                                <td valign="top" width="580" class="contentRow" style="border-collapse: collapse;padding: 40px 0 20px 0;border-bottom: 1px solid #ededed;">
                                                                                	<table style="padding: 0 1px 0 1px;">
                                                                                		<tr>
                                                                                			<td width="350" style="border-collapse: collapse;">
                                                                                				<a href="<?php echo $current_curation['link'] ?>" style = "text-decoration:none; color: #232323;">
									                                                            <h4 class="curation-heading" style="color: #202020;display: block;font-family: &quot;Futura&quot;, sans-serif;text-transform: uppercase;font-size: 18px;font-weight: normal;line-height: 130%;text-align: left;padding-right: 20px;margin-top: 0;margin-right: 0;margin-bottom: 10px;margin-left: 0;">Current Curation /<br>
									                                                            <span class="red" style="color: #c90208;"><?php echo $current_curation['heading'] ?></span></h4></a>
									                                                            <h5 style="color: #202020;display: block;font-family: &quot;Georgia&quot;, serif;font-size: 16px;font-weight: normal;line-height: 100%;text-align: left;margin-top: 0;margin-right: 0;margin-bottom: 10px;margin-left: 0;"><?php echo $current_curation['subhead'] ?></h5>
									                                                            <p style="color: #232323;font-family: &quot;Georgia&quot;, serif;font-size: 14px;line-height: 150%;text-align: left;">
									                                                            	<?php echo $current_curation['excerpt'] ?><br>
									                                                            	<a href="<?php echo $current_curation['link'] ?>" class="read_more" style="color: #232323;font-style: italic;display: block;margin-top: 20px;">Read more</a>
									                                                            </p>
								                                                        	</td>
																					 
																							 <td valign="top" width="140" style="border-collapse: collapse;">
			                                                                                	<div class="border_right curatedBy" style="height:45px; border-right: 1px solid #ededed;padding-top: 20px;padding-bottom: 10px;padding-right: 10px;color: #232323;font-family: &quot;Georgia&quot;, serif;font-size: 14px;line-height: 150%;text-align: left;">
										                                                            <div class="h7" style="color: #737373;display: block;font-family: &quot;Futura&quot;, sans-serif;font-size: 10px;text-transform: uppercase;font-weight: normal;line-height: 120%;text-align: left;margin-top: 0;margin-right: 0;margin-bottom: 5px;margin-left: 0;">curated by:</div>
										                                                            <div class="h6" style="color: #202020;display: block;font-family: &quot;Futura&quot;, sans-serif;font-size: 12px;text-transform: uppercase;font-weight: normal;line-height: 120%;text-align: left;margin-top: 0;margin-right: 0;margin-bottom: 5px;margin-left: 0;"><a href="<?php echo $current_curation['curator_link'] ?>" style = "text-decoration:none; color: #232323;"><?php echo $current_curation['curator_name'] ?><a></div> 
										                                                        </div>   
																							 </td>
																							 <td valign="top" class=" curator" style="border-collapse: collapse;padding-left: 30px;">
			                                                                                	<a href="<?php echo $current_curation['curator_link'] ?>" style="text-decoration:none; color: #232323;"><img src="<?php echo $current_curation['curator_image'] ?>" style="border: 0;height: auto;line-height: 100%;outline: none;text-decoration: none;display: inline;"> </a> 
																							 </td>
																					</tr>

																				</table>

																				 </td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td valign="top" class="contentRow" align="center" style="border-collapse: collapse;padding: 40px 0 20px 0;border-bottom: 1px solid #ededed;">
						                                                            <table class="fixed" width="100%" style="padding: 0 1px 0 1px;table-layout: fixed;">
						                                                            	<tr>
						                                                            		<?php for($i= 0; $i <= 2; $i++){ ?>
							                                                            		<td width="190" align="center" style="border-collapse: collapse;">
							                                                            			<a href="<?php echo $artworks[$i]['link'] ?>"><img src="<?php echo $artworks[$i]['image'] ?>" style="border: 0;height: auto;line-height: 100%;outline: none;text-decoration: none;display: inline;"></a>
							                                                            		</td>
						                                                            		 <?php 
																					        	}
																					        ?>
						          														</tr>
						                                                            	<tr class="subs">
						                                                            		<?php for($i= 0; $i <= 2; $i++){ ?>
						                                                            		<td style="border-collapse: collapse;padding-left: 10px;padding-top: 20px;">
						                                                            			<div class="h7" style="color: #737373;display: block;font-family: &quot;Futura&quot;, sans-serif;font-size: 10px;text-transform: uppercase;font-weight: normal;line-height: 120%;text-align: left;margin-top: 0;margin-right: 0;margin-bottom: 5px;margin-left: 0;"><a href="<?php echo $artworks[$i]['link'] ?>" style = "text-decoration:none; color: #232323;"><?php echo $artworks[$i]['title'] ?></a><br>
						                                                            				<a href="<?php echo $artworks[$i]['artistlink'] ?>" style = "text-decoration:none; color: #232323;"><?php echo $artworks[$i]['artistname'] ?></a></div>
						                                                            			<div class="h7 price" style="color: #737373;display: block;font-family: &quot;Futura&quot;, sans-serif;font-size: 10px;text-transform: uppercase;font-weight: normal;line-height: 120%;text-align: left;margin-top: 0;margin-right: 0;margin-bottom: 5px;margin-left: 0;"><?php echo $artworks[$i]['price'] ?></div>
						                                                            		</td>
						                                                            		<?php 
																					        	}
																					        ?>
						                                                            	</tr>
						                                                            </table>
						                                                            <table class="fixed artworkRow " width="100%" style="padding: 0 1px 0 1px;padding-top: 20px;table-layout: fixed;">
						                                                            	<tr>
						                                                            		<?php for($i= 3; $i <= 5; $i++){ ?>
							                                                            		<td width="190" align="center" style="border-collapse: collapse;">
							                                                            			<a href="<?php echo $artworks[$i]['link'] ?>"><img src="<?php echo $artworks[$i]['image'] ?>" style="border: 0;height: auto;line-height: 100%;outline: none;text-decoration: none;display: inline;"></a>
							                                                            		</td>
						                                                            		 <?php 
																					        	}
																					        ?>
						          														</tr>
						                                                            	<tr class="subs">
						                                                            		<?php for($i= 3; $i <= 5; $i++){ ?>
						                                                            		<td style="border-collapse: collapse;padding-left: 10px;padding-top: 20px;">
						                                                            			<div class="h7" style="color: #737373;display: block;font-family: &quot;Futura&quot;, sans-serif;font-size: 10px;text-transform: uppercase;font-weight: normal;line-height: 120%;text-align: left;margin-top: 0;margin-right: 0;margin-bottom: 5px;margin-left: 0;"><a href="<?php echo $artworks[$i]['link'] ?>" style = "text-decoration:none; color: #232323;"><?php echo $artworks[$i]['title'] ?></a><br>
						                                                            				<a href="<?php echo $artworks[$i]['artistlink'] ?>" style = "text-decoration:none; color: #232323;"><?php echo $artworks[$i]['artistname'] ?></a></div>
						                                                            			<div class="h7 price" style="color: #737373;display: block;font-family: &quot;Futura&quot;, sans-serif;font-size: 10px;text-transform: uppercase;font-weight: normal;line-height: 120%;text-align: left;margin-top: 0;margin-right: 0;margin-bottom: 5px;margin-left: 0;"><?php echo $artworks[$i]['price'] ?></div>
						                                                            		</td>
						                                                            		<?php 
																					        	}
																					        ?>
						                                                            	</tr>
						                                                            	<tr>
						                                                            		<td style="border-collapse: collapse;"></td>
						                                                            		<td align="center" height="100" style="border-collapse: collapse;">
						                                                            			<a href="<?php echo $current_curation['link'] ?>">
						                                                            				<img src="<?php echo $url?>../assets/css/img/button.jpg" style="border: 0;height: auto;line-height: 100%;outline: none;text-decoration: none;display: inline;">
						                                                            			</a>
						                                                            		</td>
						                                                            		<td style="border-collapse: collapse;"></td>
						                                                            	</tr>
						                                                            </table>
                                                                                </td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td valign="top" width="580" class="contentRow" style="border-collapse: collapse;padding: 40px 0 20px 0;border-bottom: 1px solid #ededed;">
                                                                                	<table style="padding: 0 1px 0 1px;">
                                                                                		<tr>
                                                                                			<td width="350" style="border-collapse: collapse;">
									                                                            <a href="<?php echo $last_curation['link'] ?>" style = "text-decoration:none;">
									                                                            <h4 style="padding-right: 20px;color: #202020;display: block;font-family: &quot;Futura&quot;, sans-serif;text-transform: uppercase;font-size: 18px;font-weight: normal;line-height: 130%;text-align: left;margin-top: 0;margin-right: 0;margin-bottom: 10px;margin-left: 0;">Past Curation /<br>
									                                                            <span class="red" style="color: #c90208;"><?php echo $last_curation['heading'] ?></span></h4>
									                                                        	</a>
									                                                            <h5 style="color: #202020;display: block;font-family: &quot;Georgia&quot;, serif;font-size: 16px;font-weight: normal;line-height: 100%;text-align: left;margin-top: 0;margin-right: 0;margin-bottom: 10px;margin-left: 0;"><?php echo $last_curation['subhead'] ?></h5>
									                                                            <p style="color: #232323;font-family: &quot;Georgia&quot;, serif;font-size: 14px;line-height: 150%;text-align: left;">
									                                                            	<?php echo $last_curation['excerpt'] ?> <br>
									                                                            	<a href="<?php echo $last_curation['link'] ?>" class="read_more" style="color: #232323;font-style: italic;display: block;margin-top: 20px;">Read more</a>
									                                                            </p>
								                                                        	</td>
																					 
																							 <td valign="top" width="140" style="border-collapse: collapse;">
			                                                                                	<div class="border_right curatedBy" style="height:45px; border-right: 1px solid #ededed;padding-top: 20px;padding-bottom: 10px;padding-right: 10px;color: #232323;font-family: &quot;Georgia&quot;, serif;font-size: 14px;line-height: 150%;text-align: left;">
										                                                            <div class="h7" style="color: #737373;display: block;font-family: &quot;Futura&quot;, sans-serif;font-size: 10px;text-transform: uppercase;font-weight: normal;line-height: 120%;text-align: left;margin-top: 0;margin-right: 0;margin-bottom: 5px;margin-left: 0;">curated by:</div>
										                                                            <div class="h6" style="color: #202020;display: block;font-family: &quot;Futura&quot;, sans-serif;font-size: 12px;text-transform: uppercase;font-weight: normal;line-height: 120%;text-align: left;margin-top: 0;margin-right: 0;margin-bottom: 5px;margin-left: 0;"> <a href="<?php echo $last_curation['curator_link'] ?>" style = "text-decoration:none; color: #232323;"><?php echo $last_curation['curator_name'] ?></a></div> 
										                                                        </div>   
																							 </td>
																							 <td valign="top" class=" curator" style="border-collapse: collapse;padding-left: 30px;">
			                                                                                	<a href="<?php echo $last_curation['curator_link'] ?>" style = "text-decoration:none; color: #232323;"><img src="<?php echo $current_curation['curator_image'] ?>" style="border: 0;height: auto;line-height: 100%;outline: none;text-decoration: none;display: inline;"> </a> 
																							 </td>
																					</tr>
																				</table>
																				 </td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td valign="top" width="580" class="contentRow" style="border-collapse: collapse;padding: 40px 0 20px 0;border-bottom: 1px solid #ededed;">
																					<table class="fixed artworkRow " width="100%" cellspacing="" style="padding: 0 1px 0 1px;padding-top: 20px;table-layout: fixed;">
						                                                            	<tr valign="top">
						                                                            		<?php for($i= 0; $i <= 2; $i++){ ?>
						                                                            		<td width="180" align="center" class="insights" style="border-collapse: collapse;padding-right: 10px;">
						                                                            			<div class="h7 red" style="color: #c90208;display: block;font-family: &quot;Futura&quot;, sans-serif;font-size: 10px;text-transform: uppercase;font-weight: normal;line-height: 120%;text-align: left;margin-top: 0;margin-right: 0;margin-bottom: 5px;margin-left: 0;"><a href="<?php echo $insights[$i]['category_link'] ?>" style = "text-decoration:none; color: #c90208;"><?php echo $insights[$i]['category'] ?></a></div>
						                                                            			<h4 style="color: #202020;display: block;font-family: &quot;Futura&quot;, sans-serif;text-transform: uppercase;font-size: 18px;font-weight: normal;line-height: 130%;text-align: left;margin-top: 0;margin-right: 0;margin-bottom: 10px;margin-left: 0;"><a href="<?php echo $insights[$i]['link'] ?>" style = "text-decoration:none; color: #232323;"><?php echo $insights[$i]['heading'] ?></a></h4>
						                                                            			<p style="color: #232323;font-family: &quot;Georgia&quot;, serif;font-size: 14px;line-height: 150%;text-align: left;"><?php echo $insights[$i]['excerpt'] ?>
																								<br>
									                                                            	<a href="<?php echo $insights[$i]['link'] ?>" class="read_more" style="color: #232323;font-style: italic;display: block;margin-top: 20px;">Read more</a></p>
						                                                            		</td>
						                                                            		 <?php }?>
						                                                            	</tr>
						                                                            	
						                                                            </table>

																				 </td>
                                                                            </tr>
                                                                            
                                                                        </table>
                                                                        <!-- // End Module: Standard Content \ -->
                                                            
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                		</td>
                                                    </tr>
                                                </table>                                                
                                            </td>
                                        </tr>
                                    </table>
                                    <!-- // End Template Body \ -->
                                </td>
                            </tr>
                        	<tr>
                            	<td align="center" valign="top" style="border-collapse: collapse;">
                                    <!-- // Begin Template Footer \ -->
                                	<table border="0" cellpadding="0" cellspacing="0" width="580" id="templateFooter" style="padding: 0 1px 0 1px;background-color: #FFFFFF;border-top: 0;">
                                		<tr>
                                        	<td valign="top" class="socialMedia" style="border-collapse: collapse;text-align: center;border-bottom: 2px solid #000000;padding-bottom: 20px;">
                                        		<img id="stayTuned" src="http://artuner.com.www126.your-server.de/newsletter/newsletter_stay.jpg" style="border: 0;height: auto;line-height: 100%;outline: none;text-decoration: none;padding: 20px 0 20px 0;"><br>
                                        		<a href="" style="color: #232323;"><img src="http://artuner.com.www126.your-server.de/newsletter/twitter.png" style="border: 0;height: auto;line-height: 100%;outline: none;text-decoration: none;"></a>
                                        		<a href="" style="color: #232323;"><img src="http://artuner.com.www126.your-server.de/newsletter/fb.png" style="border: 0;height: auto;line-height: 100%;outline: none;text-decoration: none;"></a>
                                        		<a href="" style="color: #232323;"><img src="http://artuner.com.www126.your-server.de/newsletter/tumblr.png" style="border: 0;height: auto;line-height: 100%;outline: none;text-decoration: none;"></a>
                                            	<a href="" style="color: #232323;"><img src="http://artuner.com.www126.your-server.de/newsletter/pin.png" style="border: 0;height: auto;line-height: 100%;outline: none;text-decoration: none;"></a>
                                               
                                            </td>
                                        </tr>
                                    	<tr>
                                        	<td valign="top" class="footerContent" style="color: #232323;font-family: georgia;font-size: 10px;text-align: center;border-collapse: collapse;">
                                            
                                               <p style="line-height:130%;">If you no longer wish to receive e-mails from Artuner, <a href="http://www.artuner.com/unsubscribe.html" style="color: #232323;">please unsubscribe here.</a><br>
                                               	Artuner Ltd., 36 Alie Street, London, UK, E1 8DA.</p>
                                            </td>
                                        </tr>
                                    </table>
                                    <!-- // End Template Footer \ -->
                                </td>
                            </tr>
                        </table>
                        <br>
                    </td>
                </tr>
            </table>
        </center>
    </body>
</html>