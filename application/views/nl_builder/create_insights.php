<h2><?php echo $subtitle ?></h2>
<?php echo validation_errors(); ?>

<?php 
$attributes = array('class' => 'create-form');
echo form_open( $url."newsletter/".$nl_id."/create_insight", $attributes) ?>
	<label for="category">Category</label>
	<input type="input" name="category" class="input" value="<?php echo set_value('category'); ?>" placeholder="Category"/>
	<label for="heading">Heading</label>
	<input type="input" name="heading"  class="input" value="<?php echo set_value('heading'); ?>" placeholder="Heading"/>
	<label for="excerpt">Excerpt</label>
	<textarea name="excerpt" class="input" placeholder="Excerpt"/><?php echo set_value('excerpt'); ?></textarea>
	<label for="link">Link to Article</label>
	<input type="input" name="link" class="input" value="<?php echo set_value('link'); ?>" placeholder="Link to Article"/>
	<div class="border-bottom"></div>
	<input type="submit" name="submit" class="button insight"  id="submit" value="Create Insight" /> 

</form>

<a href='<?php echo $url?>newsletter/<?php echo $nl_id?>' class="back" id="btn-overview"> Back to Overview </a>
