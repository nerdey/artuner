<h2><?php echo $subtitle ?></h2>

<h2>The insight has successfully been created.</h2>
<h6>All insights in this Newsletter:</h6>
<div class="border-bottom"></div>

<?php
if($insights){
    $size = sizeof($insights);
    ?>

<table class="fixed artworkRow margin-top bodyContent" width="600px" cellspacing="">
    <tr>
    <?php for($i= 0; $i <= 2; $i++){ ?>
            <td width="180" align="center" valign="top" class="insights">
                <?php if($i<$size):?>
                <h7 class="red"><a href="<?php echo $insights[$i]['category_link'] ?>"><?php echo $insights[$i]['category'] ?></a></h7>
                <h4><?php echo $insights[$i]['heading'] ?></h4>
                <p><?php echo $insights[$i]['excerpt'] ?>
                <br/>
                <a href="<?php echo $insights[$i]['link'] ?>"  class="read_more">Read more</a></p>
                <h7 class="red"><a href="<?php echo $url.'newsletter/'.$insights[$i]['id'].'/edit_insight'?>">EDIT</a></h7>
                <?php endif;?>
            </td>
        <?php 
        }
        ?>
    </tr>
    
</table>
<div class="border-bottom"></div>

<?php  if($size < 3){?>
            <a class="create add" href='<?php echo $nl_id ?>/create_insight/'>Add more Insights</a>
 <?php 
	}
	else{ 
	?>
		<p>You cannot add more that 3 Insights to the Newsletter. <br/> You can edit or delete Insights instead.</p>
	<?php 
	}
}
?>

<a href='<?php echo $url?>newsletter/<?php echo $nl_id?>' class="back" id="btn-overview"> Back to Overview </a>
